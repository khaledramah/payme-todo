import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class NoteService {

  baseUrl: string;
  userNotes: any;
  $userNotes = new Subject();
  constructor(private http: HttpClient, private authService: AuthService) {
    this.baseUrl = environment.baseUrl;
  }

  getAllNotes() {
    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.authService.token
    });

    return this.http.get(this.baseUrl + '/note', { headers: myheaders });
  }

  storeUserNotes() {
    this.$userNotes = new Subject();
    this.getAllNotes().subscribe(
      (res: any) => {
        this.userNotes = res.notes;
        this.$userNotes.complete();
      }
    )
  }

  addNote(text) {
    const data = {
      text
    };
    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.authService.token
    });

    return this.http.post(this.baseUrl + '/note', data, { headers: myheaders });
  }

}
