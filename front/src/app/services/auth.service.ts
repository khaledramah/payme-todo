import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  token: string;
  baseUrl: string;

  constructor(private http: HttpClient) {
    this.token = localStorage.getItem('token');
    this.baseUrl = environment.baseUrl;
  }

  // validateToken() {
  //   const myheaders = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'x-access-token': this.token
  //   });
  //   return this.http.post(this.baseUrl + 'employee/validate', {}, { headers: myheaders });
  // }

  login({ UserName, Password }) {
    const data = {
      userName: UserName,
      password: Password
    };
    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.post(this.baseUrl + '/user/auth', data, { headers: myheaders });
  }

}
