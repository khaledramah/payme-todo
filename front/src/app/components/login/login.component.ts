import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { LoginForm } from 'src/app/classes/login-form';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  login = new LoginForm();
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }


  onSubmit() {
    console.log(this.login)
    this.authService.login(this.login).subscribe(
      (res: any) => {
        this.authService.token = res.token;
        localStorage.setItem('token', res.token)
        this.router.navigate(['./home']);
      },
      () => {
        // handle error
      }
    )
  }
}
