import { Component, OnInit, OnDestroy } from '@angular/core';
import { NoteService } from 'src/app/services/note.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit , OnDestroy{

  notesListSubscription: Subscription;
  notes: any;
  noteText: string;
  constructor(private noteService: NoteService) { }

  ngOnInit() {
    this.noteService.storeUserNotes()
    this.notesListSubscription = this.noteService.$userNotes.subscribe(
      () => { },
      () => { },
      () => {
        this.notes = this.noteService.userNotes;
      }
    )
  }

  addNote() {
    this.noteService.addNote(this.noteText).subscribe(
      (res: any) => {
        this.noteService.userNotes = res.notes;
        this.notes = res.notes;
      }
    )
  }

  ngOnDestroy() {
    if (this.notesListSubscription) { this.notesListSubscription.unsubscribe(); }
  }

}