const bcrypt = require('bcrypt');
const config = require('../config');
const jwt = require('jsonwebtoken');
const saltRounds = 10;

const hashPassword = async (password) => {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, saltRounds, (error, hashedPassword) => {
            if (error) reject(error)
            resolve(hashedPassword)
        })
    })
}

const verifyPassword = async (password, hashedPassword) => {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, hashedPassword, (error, same) => {
            if (error) reject(error)
            resolve(same)
        })
    })
}

const newToken = options => {
    return jwt.sign(options, config.secrets.jwt, {
        expiresIn: config.secrets.jwtExp
    })
}

const verifyToken = token =>
    new Promise((resolve, reject) => {
        jwt.verify(token, config.secrets.jwt, (err, payload) => {
            if (err) return reject({ code: 401, message: 'invalide token' })
            resolve(payload)
        })
    })

const runAsyncWrapper = callback => {
    return function (req, res, next) {
        callback(req, res, next)
            .catch(next)
    }
}

module.exports = {
    runAsyncWrapper,
    hashPassword,
    verifyPassword,
    newToken,
    verifyToken
}