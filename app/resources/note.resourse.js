const { getAllNotes, createTodoNote } = require('../models/user.model')

const addNote = async (userId, { text }) => {
    if (!text || text === '')
        throw { code: 400, message: 'can not add empty note' }

    await createTodoNote(userId, text)

    return await getAllnotes(userId);
}

const getAllnotes = async (userId) => {
    return await getAllNotes(userId)
}

module.exports = {
    addNote,
    getAllnotes
}