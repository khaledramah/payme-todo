const { hashPassword, newToken, verifyPassword } = require('../util')
const { createUser, updateUsertoken, getByUserName, getAllNotes, createTodoNote } = require('../models/user.model')

const registerUser = async ({ userName, password }) => {
    if (!userName || !password)
        throw { code: '400', message: 'user name or password was not in corect formate' }

    password = await hashPassword(password);

    const user = await createUser({ userName, password })

    const token = newToken({ userId: user._id })
    await updateUsertoken({ token, id: user._id })
    // await createTodoNote(user._id, 'note 1')
    // await createTodoNote(user._id, 'note 2')
    // await createTodoNote(user._id, 'note 3')

    return token;
}

const login = async ({ userName, password }) => {
    if (!userName || !password)
        throw { code: 401, message: 'Invalide UserName or password' }

    const user = await getByUserName(userName);
    if (!user)
        throw { code: 401, message: 'Invalide UserName or password' }

    if (await verifyPassword(password, user.password)) {
        const token = newToken({ userId: user._id })
        await updateUsertoken({ token, id: user._id })
        // console.log((await getAllNotes(user._id)));
        return token;
    } else {
        throw { code: 401, message: 'Invalide UserName or password' }
    }
}

module.exports = Object.freeze({
    registerUser,
    login
})
