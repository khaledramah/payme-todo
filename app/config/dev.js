module.exports = Object.freeze({
  config: {
    secrets: {
      jwt: 'verystrongsecretkey'
    },
    dbUrl: 'mongodb://localhost:27017/todoappdb'
  }
})
