const mongoose = require('mongoose')
const options = require('../config')

module.exports = Object.freeze({
  connect: (url = options.dbUrl, opts = {}) => {
    return mongoose.connect(
      url,
      { ...opts, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify : false}
    )
  }
})
