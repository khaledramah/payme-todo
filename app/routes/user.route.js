const { Router } = require('express')
const userResourse = require('../resources/user.resourse')
const { runAsyncWrapper } = require('../util')
const router = Router()

router.post('/', runAsyncWrapper(async (req, res) => {
    const token = await userResourse.registerUser(req.body)
    res.send({ tokrn: token });
}));

router.post('/auth', runAsyncWrapper(async (req, res) => {
    debugger;
    const token = await userResourse.login(req.body)
    res.send({ token: token });
}));


// router.post('/validate', (req, res) => {
//     const token = req.headers['Authorization'] ? req.headers['Authorization'] : '';
//     await userResourse.validateToken(token)
//     res.status(201).send();
// });

module.exports = router
