const { Router } = require('express')
const noteResourse = require('../resources/note.resourse')
const { runAsyncWrapper, verifyToken } = require('../util')
const router = Router()

router.post('/', runAsyncWrapper(async (req, res) => {
    const token = req.headers['authorization'] ? req.headers['authorization'] : '';
    const { userId } = await verifyToken(token);
    const userNotes = await noteResourse.addNote(userId, req.body)
    res.status(200).send(userNotes);
}));

router.get('/', runAsyncWrapper(async (req, res) => {
    const token = req.headers['authorization'] ? req.headers['authorization'] : '';
    debugger;
    const { userId } = await verifyToken(token);
    const userNotes = await noteResourse.getAllnotes(userId)
    res.status(200).send(userNotes);
}));

module.exports = router
