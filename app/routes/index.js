const { Router } = require('express')
const userRouter = require('./user.route')
const noteRouter = require('./note.route')

const router = Router()

router.use('/user', userRouter)
router.use('/note', noteRouter)


module.exports = router