const cors = require('cors')
const path = require('path');
const express = require('express')
const { json, urlencoded } = require('body-parser')

const config = require('./config')
const routes = require('./routes')
const { connect } = require('./db')

const app = express()


app.use(cors())
app.use(json())
app.use(urlencoded({ extended: true }))

app.use('/api', routes)

app.use(express.static(__dirname + './../front/dist/todo'));

// Send all requests to index.html
app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname + './../front/dist/todo/index.html'));
});


app.use((err, req, res, next) => {
  console.log(err)
  if (err.code)
    res.status(err.code).send(err.message)
  else
    res.status(500).send(err)
})

module.exports = start = async () => {
  try {
    await connect()
    app.listen(config.port, () => {
      console.log(`REST API on http://localhost:${config.port}`)
    })
  } catch (e) {
    console.error(e)
  }
}