const mongoose = require('mongoose')
var noteSchema = mongoose.Schema({

    text: String,
    createdAt: Date
}, { _id: false });

const userSchema = new mongoose.Schema({
    userName: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: false
    },
    notes: [noteSchema]
});

const userModel = mongoose.model('user', userSchema)

const createUser = async (user) => {
    return await userModel.create(user)
}

const updateUsertoken = async ({ token, id }) => {
    return await userModel.updateOne({ _id: id }, { $set: { token } })
}

const getByUserName = async (userName) => {
    return await userModel.findOne({ userName })
}

const createTodoNote = async (id, text) => {
    return await userModel.findOneAndUpdate({ _id: id }, { $push: { notes: { text, createdAt: Date.now() } } })
}

const getAllNotes = async (id) => {
    return await userModel.findOne({ _id: id }, { _id: 0, notes: 1 })
}

module.exports = Object.freeze({
    createUser,
    updateUsertoken,
    getByUserName,
    createTodoNote,
    getAllNotes
})
